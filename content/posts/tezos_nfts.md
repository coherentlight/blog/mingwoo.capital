---
title: "Tezos NFTs: An Opinionated Guide"
date: 2021-09-23
tags: ["tezos", "nfts"]
---
This week we spent some time diving into the Tezos NFT ecosystem - these
are our notes.

# Getting Started
Basically everything just works!

* [Wallet](https://templewallet.com/)
* [Block Explorer](https://tzkt.io)
* [Auction House](https://objkt.com/)

You can obtain XTZ on all major exchanges except FTX.

# Collections
Some of our favorite collections are listed below (caveat: after a few
days of looking).
We've included most of the major (high price) collections, but left out some
that we subjectively just don't enjoy (Tezos Pirates).

## Emergents TCG

{{< figure src="/img/nfts/zoemg.png" width="512" >}}

We originally got interested in Tezos NFTs because of InterPop's 
[Emergents TCG](https://www.interpop.io/projects/emergents-trading-card-game/),
an upcoming dueling card game designed by several MtG hall-of-famers,
and in our opinion the most promising blockchain game so far (from a
gameplay point of view).
There are currently six promo cards [available](https://objkt.com/profile/tz1gwyK75gqpnm5oPvQ9idaGBA93JRsVzLMJ/creations)
as well as a selection of concept art and such.

## Iconic Tezos NFTs
{{< figure src="/img/nfts/tezzards_503.png" width="256" >}}

[Tezzards](https://objkt.com/profile/tz1LLPWMyZ7gKsp3WnLfemyAYW6CoZoozku5/creations)
are the most popular pfp in the Tezos part of CT.
They grow on you :innocent:.
There's also also [Tezonthropophagy](https://objkt.com/profile/tz1hXXg87wXDN4qVEcuypgYt2BfKaUXqgqcW/creations),
which make for a nice banner to go with a Tezzard.

{{< figure src="/img/nfts/avatar_551.png" width="256" >}}

[PRJKTNEON](https://objkt.com/profile/tz1UY2DY53YPKXTo4TeS5XcK2hK2A1d3ut2D/creations)
is an amazing collection of cyberpunk avatars which are highly
sought-after. 
We check objkt.com for new auctions every morning :wink:. 
Owners also get lore pages airdropped on occasion.

## Punks
Tezos has many punk knock-offs, several of which we quite like:

| Collection                                                                                   | NFT                                                                |
|----------------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| [TezPunks](https://objkt.com/profile/tz1P1umUcd3QHFcU6fsmxewGFBfCNXvLJzHT/creations)         | {{< figure src="/img/nfts/tezpunks_0430.png" width="128" >}}       |
| [Parallel Punks](https://objkt.com/profile/tz1goDsJjcJPjkd2eBcktFkgBRa8gRNMZSeM/creations)   | {{< figure src="/img/nfts/parallelpunks_160.png" width="128" >}}   |
| [Underworld Punks](https://objkt.com/profile/tz1TC1yW47cgHp4hi2aXG7NfTikXrQ9qPAbp/creations) | {{< figure src="/img/nfts/underworldpunk_0504.png" width="128" >}} |
| [Punkz](https://objkt.com/profile/tz1ddVbmkzALufXA7yugERg98tGfxvCDAZRj/creations)            | {{< figure src="/img/nfts/punkz_350.png" width="128" >}}           |

## Punklikes
| Collection                                                                                 | NFT                                                             |
|--------------------------------------------------------------------------------------------|-----------------------------------------------------------------|
| [CryptoLLamas](https://objkt.com/profile/tz1fhZzkEdKA6DQjJwymWFT7Vo6Zif2bhYQx/creations)   | {{< figure src="/img/nfts/cryptollamas_025.png" width="128" >}} |
| [Tezos Dogs](https://objkt.com/profile/tz1ad39TjPjHpCfauYCwrfA52UkhFansdtME/creations)     | {{< figure src="/img/nfts/tezosdogs_0278.png" width="128" >}}   |
| [Paper Bag Head](https://objkt.com/profile/tz1SyysWSn9WJyK4rmpfn7zGmbBUVbaLnuqT/creations) | {{< figure src="/img/nfts/paperbaghead_031.png" width="128" >}} |

## Apes
Likewise, there are a few ape collections on Tezos. We particularly 
:heart: the LegoApes!

| Collection                                                                                 | NFT                                                             |
|--------------------------------------------------------------------------------------------|-----------------------------------------------------------------|
| [LegoApes](https://objkt.com/profile/tz1bnJyBprVYZQayW68rdUQRJ97aUEmfwkoh/creations)       | {{< figure src="/img/nfts/legoape_4.jpg" width="128" >}}        |
| [Kongz](https://objkt.com/profile/tz1UsmnMTtex7afAFGVravD3NQuQ8WfpEWDB/creations)          | {{< figure src="/img/nfts/kongz_058.jpg" width="128" >}}        |
| [Tezos Ape Club](https://objkt.com/profile/tz1UHjsKdtCBpQpHtv4DatkaBVsnwWuf7k1Y/creations) | {{< figure src="/img/nfts/tezosapeclub_314.png" width="128" >}} |

## GIFs
Tezos has a ton of super-cute pixel animal GIFs!

| Collection                                                                            | NFT                                                             |
|---------------------------------------------------------------------------------------|-----------------------------------------------------------------|
| [Bunny Knights](https://bunnyknights.com)                                             | {{< figure src="/img/nfts/bunnyknights_420.gif" width="128" >}} |
| [Catezos](https://catezos.xyz)                                                        | {{< figure src="/img/nfts/catezos_173.gif" width="128" >}}      |
| [Pinguix](https://objkt.com/profile/tz1fHQuSVKrL292fJYAhvPPVBvUGA4cR3YvV/creations)   | {{< figure src="/img/nfts/pinguix_pirate.gif" width="128" >}}   |
| [Ghostix](https://objkt.com/profile/tz1cSqd1MqBiCGt1oB1MtBHVvVm7iJw1tGvV/creations)   | {{< figure src="/img/nfts/ghostix_zombie.gif" width="128" >}}   |
| [Foxy](https://objkt.com/profile/tz1fcUXUqCAFYi3mNXaodz8dLUjQcwG1gFa9/creations)      | {{< figure src="/img/nfts/foxy_eevee.gif" width="128" >}}       |
| [Panda](https://objkt.com/profile/tz1Uj81aMk7tuq3EizGxypsi5vDyBj4PhCD9/creations)     | {{< figure src="/img/nfts/panda_sulley.gif" width="128" >}}     |
| [Skeletons](https://objkt.com/profile/tz1ddexdNd3RXSwt6Wp7tjUESnG5KUgDFtDH/creations) | {{< figure src="/img/nfts/rick.gif" width="128" >}}             |
| [Potatoes](https://objkt.com/profile/tz1TJXDDdMYaApMgVvEm7svPkg1XrP7NQ1U5/creations)  | {{< figure src="/img/nfts/pooh.gif" width="128" >}}             |

## Pixel Art
| Collection                                                                             | NFT                                                            |
|----------------------------------------------------------------------------------------|----------------------------------------------------------------|
| [CryptoBugs](https://objkt.com/profile/tz1XYfw6zypurrE4xmz53Bcde3azFopYH1uR/creations) | {{< figure src="/img/nfts/cryptobugs_0033.png" width="128" >}} |
| [CryptoPygz](https://objkt.com/profile/tz1KqDqSufNWWiHJ6f3bWpkuDbETi2o8wsPo/creations) | {{< figure src="/img/nfts/cryptopygz_13.png" width="128" >}}   |
| [TezHens](https://objkt.com/profile/tz1fngBbafkeRagkNM8kXBuQqYSDrAfmQQok/creations)    | {{< figure src="/img/nfts/tezhen_0117.gif" width="128" >}}     |

## Anime
| Collection                                                                            | NFT                                                         |
|---------------------------------------------------------------------------------------|-------------------------------------------------------------|
| [Ch=mpathy](https://objkt.com/profile/tz1L73X7FjAUzqPfV5YE2nLAXEpcaKJBXQuL/creations) | {{< figure src="/img/nfts/chmpathy_117.png" width="128" >}} |
| [Pandionic](https://objkt.com/profile/tz1cV9PcxPg4Tf1TQwN6DjxrPKioXec4NmM4/creations) | {{< figure src="/img/nfts/pandionic_1.jpg" width="128" >}}  |
| [Onimata](https://objkt.com/profile/tz1VbGPnW8bRzaYyupjUF2qLV9s5f3p1KsQZ/creations)   | {{< figure src="/img/nfts/onimata_08.jpg" width="128" >}}   |

# Acquisition Strategy
We've developed a two-stage approach to NFT acquisition:
1. Once you've identified a promising collection, sweep the floor, 
   i.e. buy everything around the lowest available price (this really
   wants to be automated).
2. Inspect the higher-priced items visually. Look inside yourself. 
   If you see one that sparks joy, buy.

Most NFTs have no trading history so it's basically impossible to 
know what to offer when there is no price (overall price range is
currently 1-10k XTZ fwiw).
Auctions seem to run for around 24h usually, so just going through the
list of currently running auctions once a day kinda works.

# Feature Requests
We would love to see all of the following implemented on objkt.com:

* Auctions: finding and monitoring ongoing auctions is somewhat tedious.
  Notifications would be awesome.
* Discovery: Objkt.com currently represents NFT marketplaces like 
  [H=N](https://www.hicetnunc.xyz/) as a single top-level collection, 
  which makes it annoying to find individual collections which 
  originated on that site.
* Statistics: summary stats for recent trading activity would be great
  to have (number of trades, floor price over time, etc.)
* Batch operations: managing many offers is a very time consuming process -
  would be great to be able to batch operations to reduce time spent 
  waiting for block confirmation.
* Gallery: we would love to be able to arrange our collection in
  different ways for viewing.

Comments appreciated on [Twitter](https://twitter.com/0xCOHERENTLIGHT/status/1441097698808008708)
or [Reddit](https://www.reddit.com/r/tezos/comments/pu0pvj/tezos_nfts_an_opinionated_guide/).
